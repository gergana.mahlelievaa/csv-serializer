#include <stdio.h>
//#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <conio.h>
#include <getopt.h>
#include "logger.h"
#include "serialize.h"
#include "deserialize.h"
//0 false-1true

struct SalesProgramArg{
    int silent;
    char *filepath;
   char  *mode;
}appArgs;

int main(int argc,char **argv)
{
    int opt;

    while ((opt = getopt(argc, argv, "sf:c:")) != -1) {
        switch (opt) {
            case 's':
                appArgs.silent = 1;
                break;
            case 'f':
                appArgs.filepath = malloc(strlen(optarg)+1);
                strcpy(appArgs.filepath,optarg);

                break;
            case 'c':
              // appArgs.mode=atoi(optarg);
             appArgs.mode=optarg;
                break;
            default: /* '?' */
                fprintf(stderr, "Usage: %s [-s] [-f filepath] [-c mode] \n",argv[0]);
                exit(2);
        }
    }

    LOG("silent=%d; filepath=%s; mode=%s; \n",appArgs.silent,appArgs.filepath,appArgs.mode);

    FILE *fp;

    fp= fopen(appArgs.filepath,"r");
    if(fp==NULL){
        printf("ERROR!");
        exit(2);
    }

    if (strcmp(appArgs.mode, "serialize") == 0){
        serialize(fp);
    }
    if (strcmp(appArgs.mode, "deserialize") == 0){
        deserialize(fp);
    }




/*if(appArgs.mode==1){
    serialize(fp);
}
if(appArgs.mode==2){
    deserialize(fp);
}
if(appArgs.mode!=1 || appArgs.mode!=2){
    exit(2);
}*/
    //deserialize(fp);

    return 0;
}

