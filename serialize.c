#include <stdio.h>
#include <string.h>
#include "datatypes.h"
#define SERIALIZE_HEADER_FIELD(field) \
    headerfields.field[strlen(headerfields.field) - 1] = '\0'; \
    fprintf(outfile,"%s,",&headerfields.field[1]);

#define SERIALIZE_DATE_FIELD_STR(field) \
    datafields.field[strlen(datafields.field) - 1] = '\0'; \
    fprintf(outfile,"%s,",&datafields.field[1]);


int serialize(FILE *file){
    if(file==NULL){
        return -1;
    }
    char line[256];
    FILE *outfile;
    outfile= fopen("serialize.bin","wb");

    if(fgets(line,sizeof (line),file)){
        size_t lineLen = strlen(line);
        if(line[lineLen-1] != '\n'){
            //print error
            return  -1;
        }
        sscanf(line,"%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%s",headerfields.row,
               headerfields.orderID,headerfields.orderDate,headerfields.customerID,
               headerfields.city,headerfields.state,headerfields.postalCode,headerfields.region,
               headerfields.productID,headerfields.category,headerfields.subCategory,headerfields.price);
        printf("Price:%s\n",headerfields.subCategory);
        printf("Price:%s\n",headerfields.price);

        SERIALIZE_HEADER_FIELD(row);
        SERIALIZE_HEADER_FIELD(orderID);
        SERIALIZE_HEADER_FIELD(orderDate);
        SERIALIZE_HEADER_FIELD(customerID);
        SERIALIZE_HEADER_FIELD(city);
        SERIALIZE_HEADER_FIELD(state);
        SERIALIZE_HEADER_FIELD(postalCode);
        SERIALIZE_HEADER_FIELD(region);
        SERIALIZE_HEADER_FIELD(productID);
        SERIALIZE_HEADER_FIELD(category);
        SERIALIZE_HEADER_FIELD(subCategory);
        fwrite(&headerfields.price[1], strlen(headerfields.price)-2,1,outfile);

        fprintf(outfile,"\n");
    }

    while(fgets(line,sizeof (line),file)){
        sscanf(line,"%d,%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%lf",&datafields.row,
              datafields.orderID,datafields.orderDate,datafields.customerID,
             datafields.city, datafields.state, &datafields.postalCode, datafields.region,
               datafields.productID, datafields.category, datafields.subCategory, &datafields.price);


        fprintf(outfile,"%d,",datafields.row);
        SERIALIZE_DATE_FIELD_STR(orderID);
        SERIALIZE_DATE_FIELD_STR(orderDate);
       // fprintf(outfile,"%s,",&datafields.orderDate);
        SERIALIZE_DATE_FIELD_STR(customerID);
        SERIALIZE_DATE_FIELD_STR(city);
        SERIALIZE_DATE_FIELD_STR(state);
        SERIALIZE_DATE_FIELD_STR(postalCode);
      // fprintf(outfile,"%s,",&datafields.postalCode);
        SERIALIZE_DATE_FIELD_STR(region);
        SERIALIZE_DATE_FIELD_STR(productID);
        SERIALIZE_DATE_FIELD_STR(category);
        SERIALIZE_DATE_FIELD_STR(subCategory);
        fprintf(outfile,"%.2lf",datafields.price);
        fprintf(outfile,"\n");

    }

    fclose(outfile);
}
