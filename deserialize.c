#include <stdio.h>
#include <string.h>
#include "datatypes.h"
#define DESERIALIZE_HEADER_FIELD(field) \
          fprintf(outfile,"%s,",headerfields.field);

#define DESERIALIZE_DATE_FIELD_STR(field) \
fprintf(outfile,"%s,",datafields.field);


int deserialize(FILE *file){
    if(file==NULL){
        return -1;
    }
    char line[256];
    FILE *outfile;
    outfile= fopen("deserialize.csv","w");

   if(fgets(line,sizeof (line),file)) {
       sscanf(line, "%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%s", headerfields.row,
              headerfields.orderID, headerfields.orderDate, headerfields.customerID,
              headerfields.city, headerfields.state, headerfields.postalCode, headerfields.region,
              headerfields.productID, headerfields.category, headerfields.subCategory, headerfields.price);
       printf("Price:%s\n", headerfields.subCategory);
       printf("Price:%s\n", headerfields.price);

       fprintf(outfile,"%s,",headerfields.row);
       DESERIALIZE_HEADER_FIELD(orderID);
       DESERIALIZE_HEADER_FIELD(orderDate);
       DESERIALIZE_HEADER_FIELD(customerID);
       DESERIALIZE_HEADER_FIELD(city);
       DESERIALIZE_HEADER_FIELD(state);
       DESERIALIZE_HEADER_FIELD(postalCode);
       DESERIALIZE_HEADER_FIELD(region);
       DESERIALIZE_HEADER_FIELD(productID);
       DESERIALIZE_HEADER_FIELD(category);
       DESERIALIZE_HEADER_FIELD(subCategory);
       fprintf(outfile,"%s",headerfields.price);
       fwrite("\n", 1, 1, outfile);
   }

    while(fgets(line,sizeof (line),file)){

        sscanf(line,"%d,%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%lf",&datafields.row,
               datafields.orderID,datafields.orderDate,datafields.customerID,
               datafields.city, datafields.state, &datafields.postalCode, datafields.region,
               datafields.productID, datafields.category, datafields.subCategory, &datafields.price);
        fprintf(outfile,"%d,",datafields.row);
        DESERIALIZE_DATE_FIELD_STR(orderID);
       // fprintf(outfile,"%s,",datafields.orderDate);
        DESERIALIZE_DATE_FIELD_STR(orderDate);
        DESERIALIZE_DATE_FIELD_STR(customerID);
        DESERIALIZE_DATE_FIELD_STR(city);
        DESERIALIZE_DATE_FIELD_STR(state);
   //     fprintf(outfile,"%s,",datafields.postalCode);
        DESERIALIZE_DATE_FIELD_STR(postalCode);
        DESERIALIZE_DATE_FIELD_STR(region);
        DESERIALIZE_DATE_FIELD_STR(productID);
        DESERIALIZE_DATE_FIELD_STR(category);
        DESERIALIZE_DATE_FIELD_STR(subCategory);
        fprintf(outfile,"%.2lf",datafields.price);
        fprintf(outfile,"\n");

    }
    fclose(outfile);
}
