//
// Created by gerga on 15.4.2023 г..
//

#ifndef UNTITLED_DATATYPES_H
#define UNTITLED_DATATYPES_H
#define HEADER_FIELD_SIZE 32
#define DATA_FIELD_SIZE 128
struct SalesHeader{
    char row[HEADER_FIELD_SIZE];
    char orderID[HEADER_FIELD_SIZE];
    char orderDate[HEADER_FIELD_SIZE];
    char customerID[HEADER_FIELD_SIZE];
    char city[HEADER_FIELD_SIZE];
    char state[HEADER_FIELD_SIZE];
    char postalCode[HEADER_FIELD_SIZE];
    char region[HEADER_FIELD_SIZE];
    char productID[HEADER_FIELD_SIZE];
    char category[HEADER_FIELD_SIZE];
    char subCategory[HEADER_FIELD_SIZE];
    char price[HEADER_FIELD_SIZE];
}static headerfields;
struct SalesDate{
    int row;
    char orderID[DATA_FIELD_SIZE];
    char orderDate[DATA_FIELD_SIZE];
    char customerID[DATA_FIELD_SIZE];
    char city[DATA_FIELD_SIZE];
    char state[DATA_FIELD_SIZE];
    char postalCode[DATA_FIELD_SIZE];
    char region[DATA_FIELD_SIZE];
    char productID[DATA_FIELD_SIZE];
    char category[DATA_FIELD_SIZE];
    char subCategory[DATA_FIELD_SIZE];
    double price;
}static datafields;

#endif //UNTITLED_DATATYPES_H
